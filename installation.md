# SIBYL INSTALLATION

> **Note:** This is a document part of the [SIBYL documentation](/README.md)

## Background

**SIBYL** is a software application of the **ARIX Suite** for Analysis of Robot Inspections under isolation. It is

SIBYL is provided with a semi-automatic deployment process based on [Docker](https://www.docker.com/)

This process requires some manual environment settings that the user has to define according the following instructions.

Please, first to all, check you environment meets all [SIBYL system pre-conditions](/preconditions.md).

---
## Pre-conditions

* Windows10-11 users must have a [Terminal](https://docs.microsoft.com/en-us/windows/terminal/install) with [Linux Bash Terminal](https://www.microsoft.com/en-us/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab) installed. 
* Windows10-11 users has to install and activate [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install).
* All users must have [Docker installed](https://docs.docker.com/engine/install/).
* All users must have [docker-compose installed](https://docs.docker.com/compose/install/).
* We recommend to [install Docker Desktop for Windows and Mac users](https://www.docker.com/products/docker-desktop).
* Consider to install any suitable IDE or code editor such as [Visual Studio Code](https://code.visualstudio.com/).

---

## Installation procedure

*Using a Lnux Bash terminal* 

1. **Login in the GitLab Sibyl repo** at the following address [https://gitlab.sibyl.arix-tech.com/SIBYL/docker-compose_build/-/tree/develop](https://gitlab.sibyl.arix-tech.com/SIBYL/docker-compose_build/-/tree/develop)
<br>
2. **Add your SSH key in GitLab** at your profile Personal Settings / SSH Keys, to be able to cloning the repo locally.
<br>
3. **Clone the branch "develop"** of **"docker-compose_build"** project in your local repository.
<br>
4. Access to your local repository using Bash, and **copy the .env.dist into .env**: `cp .env.dist .env`
<br>
5. **Generate some random strings** with `pwgen -s 40`, that you'll use later on to fill the `.env`file.
<br>
6. **Edit the environment variables file** `.env` file with any editor like VS Studio Code, but be careful you must use **consistent Line Breaks configuration** in VS Code (**using LF instead CRLF**).
<br>
    - **Set the APP_HOST_ROOT value**. Thant can be be the root to your remote server container (i.e. `demo.sibyl.arix-tech.com`) or for local deployments: `sibyl-next.docker`

        ```
        APP_HOST_ROOT=sibyl-next.docker
        ```
    - **Set variables for [MYSQL](https://www.mysql.com/)** as follow:
        ```
        MYSQL_HOSTNAME=database.sibyl
        MYSQL_RANDOM_ROOT_PASSWORD=yes
        MYSQL_ONETIME_PASSWORD=yes
        MYSQL_ROOT_PASSWORD=
        MYSQL_DATABASE=sibyl
        MYSQL_USERNAME=sibyl
        MYSQL_PASSWORD=uTh4phaith0phee6Ahweeda5shooPeima1gaikoh
        ```

    - **And DATABASE_MIGRATIONS**:
        ```
        DATABASE_MIGRATIONS_HOSTNAME=database-migrations.sibyl
        ```

    - **Set the value for the [JWT](https://jwt.io/) Token secret**:
        ```
        JWT_SECRET=jaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a
        ```

    - **Now configure a user to generate assets** such as PDFs:
        ```
        USER_ASSET_GENERATION_WORKER_USERNAME=worker
        USER_ASSET_GENERATION_WORKER_PASSWORD=WorkerPass1!
        ```

    - **Now configure [MINIO](https://min.io/)** *(a distributed object storage server)*, by adding some kes and pass phrases:

        ```
        MINIO_HOSTNAME=storage.sibyl
        MINIO_PUBLIC_SUBDOMAIN=storage
        MINIO_ADMIN_ACCESS_KEY=qaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a
        MINIO_ADMIN_SECRET_KEY=qaiy9sei4ainu6waiqu3ozie7eac1Hierao3Siod
        MINIO_CONSOLE_ACCESS_KEY=jaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a
        MINIO_CONSOLE_SECRET_KEY=saiy9sei4ainu6waiqu3ozie7eac1Hierao3Siod
        MINIO_CONSOLE_PBKDF_PASSPHRASE=vaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a
        MINIO_CONSOLE_PBKDF_SALT=vaiy9sei4ainu6waiqu3ozie7eac1Hierao3Siod
        ```

    - **Continue defining the MINION'S BUCKET names**:
<br>
        > *Be careful, because bucket names should not contain + sings since they will be URL encoded.*

        ```
        MINIO_UNPACK_BUCKET=unpack
        MINIO_UPLOADS_BUCKET=uploads-v2
        MINIO_REPORTS_BUCKET=reports
        ```

    - **Add the MINIO_CONSOLE_HOSTNAME** and the **MINION_CONFIGURATION_HOSTNAME**:
        ```
        MINIO_CONSOLE_HOSTNAME=minio-console.sibyl

        MINIO_CONFIGURATOR_HOSTNAME=minio-configurator.sibyl
        ```
    - Now, it is time to continue the edition of the `.env` file to add the Message Broker config ACTIVEMQ.

        ```
        ACTIVEMQ_HOSTNAME=activemq.sibyl
        ACTIVEMQ_WS_PUBLIC_SUBDOMAIN=ws
        ACTIVEMQ_ADMIN_PASSWORD=yurVkgp3GiQ1yFmkfyF3E2zgQ3HjopJMj8aGkisi
        ACTIVEMQ_API_USERNAME=sibyl
        ACTIVEMQ_API_PASSWORD=yurVkgp3GiQ1yFmkfyF3E2zgQ3HjopJMj8aGkisi
        ACTIVEMQ_MINIO_USERNAME=sibyl
        ACTIVEMQ_MINIO_PASSWORD=yurVkgp3GiQ1yFmkfyF3E2zgQ3HjopJMj8aGkisi
        ACTIVEMQ_VT_FILE_BUCKET_EVENTS=VirtualTopic.FileBucketEvents
        ACTIVEMQ_VT_FILE_BUCKET_EVENTS_CQ_FILE_EVENTS_LISTENER=Consumer.FileEventsListener.VirtualTopic.FileBucketEvents
        ACTIVEMQ_VT_FILE_EVENTS=VirtualTopic.FileEvents
        ACTIVEMQ_VT_FILE_EVENTS_CQ_DATA_IMPORT=Consumer.DataImport.VirtualTopic.FileEvents
        ACTIVEMQ_VT_PACKAGE_PROCESSING_EVENTS=VirtualTopic.PackageProcessingEvents
        ACTIVEMQ_VT_PACKAGE_PROCESSING_EVENTS_CQ_DATA_IMPORT=Consumer.DataImport.VirtualTopic.PackageProcessingEvents
        ACTIVEMQ_Q_PACKAGE_PROCESSING_STEPS_SCHEDULE=PackageProcessingStepsSchedule
        ACTIVEMQ_Q_ASSETS_GENERATION_SCHEDULE=AssetsGenerationSchedule
        ```

    - Continue the edition of rgw `.env` file, now to **add the configuration of the Application Server NGINX**:

        ```
        NGINX_HOSTNAME=nginx.sibyl
        ```

    - **Add the SECURITY_SERVICE_HOSTNAME** and **SECURITY_SERVICE_PUBLIC_SUBDOMAIN**
        ```
        SECURITY_SERVICE_HOSTNAME=security-service.sibyl
        SECURITY_SERVICE_PUBLIC_SUBDOMAIN=security
        ```

    - **Include the REST_API_HOSTNAME** and **REST_API_PUBLIC_SUBDOMAIN**:
        ```
        REST_API_HOSTNAME=rest-api.sibyl
        REST_API_PUBLIC_SUBDOMAIN=api
        ```        

    - Finally, complete the `.env` file variables definition with the **FILE_ACCESS**, and host names for the following services: **FILE_EVENTS**, **DATA_IMPORT**, **ASSETS_GENERATION_SERVICE, and **UI_APPLICATION** with following values:
        ```
        FILE_EVENTS_LISTENER_HOSTNAME=file-events-listener.sibyl

        DATA_IMPORT_PROCESS_MANAGER_HOSTNAME=data-import-process-manager.sibyl

        DATA_IMPORT_WORKERS_HOSTNAME=data-import-workers.sibyl

        ASSETS_GENERATION_SERVICE_HOSTNAME=assets-generation-service.sibyl

        UI_APPLICATIONS_HOSTNAME=ui-applications.sibyl
        ```

        > ***Be sure the `.env` ends with a blank line** , because it will concatenate to another file by the sh scripts. Otherwise it can cause errors during the Docker building procedure.*
        
<br>

7. Then, our environment configuration file is ready to use. Now it is time to **login in Docker with our user account**.


    > *If you don´t have valid user, go to Docker webpage and proceed with the registration, following the instructions on the screen. Remember the username and password.*
<br>
    - Follow this instruction for **creating a Linux user and adding it to the Docker group**:
        - Create the user with name *"dev"* using the root user `sudo su`*(enter the root's password)*
        - Add a new user using the command `adduser dev`and set his password with `passwd dev`*(enter any password)*
        - Add the `dev`user to the `Docker`group: `usermod -a -G docker dev`
<br>
9. Using the user of Docker group (eventually, `su dev`) at the `/docker-compose_build` folder. **Login to the registry of gitlab** using this command: `docker login registry.gitlab.sibyl.arix-tech.com`
<br>
10. **Use the same credentials you used at GitLab for cloning the repo** (i.e. `username@lurtis.com` and that his password) to login.
<br>
11. Now you are OK to **build the container by using the sh scripts** at the `scripts` folder.
<br>
    > ***Do no use `docker build`. It doesn't work.***


    Instead of that, from `/docker-compose_build$`, use the sh script command: `scripts/up --build && scripts/post-create`


> *Docker now will download the corresponding images from the repo and related software used by the Sibyl system described in the `.env-versions` file and configure them using the `.env` file values, using the `docker-compose`receipt.*

> Please, note that the log will end with an automatically generated **password** and **username**. You should see something like this in the console:
```
User created [username:worker][password:WorkerPass1!].
```
***Do not miss it!***

<br>
12. If you are **using Windows for a local deployment**, you have to perform an additional step to configure your localhost in order to have a proper mapping of service namespaces and avoid CORSS errors by your browser security.
<br>

- **modify your host file to include the following mappings**:
    ```
    127.0.0.1 sibyl-next.docker
    127.0.0.1 api.sibyl-next.docker
    127.0.0.1 file-access.sibyl-next.docker
    127.0.0.1 security.sibyl-next.docker
    127.0.0.1 ws.sibyl-next.docker
    127.0.0.1 storage.sibyl-next.docker
    127.0.0.1 activemq.sibyl-next.docker
    ```
---
## Using the SIBYL application
Open your Internet browser and enter the following address: http://sibyl-next.docker/
> *Note: some browsers require to include `http://`* 

<br>
### Refinery initial setup
If it is the first time you enter in the application, you have to **proceed with the setup of the refinery**
*(note: current software version doesn't support multi-tenant so every instance works only with a refinery)*

Enter the **refinery name**, the **address of the refinery**, and its location by that address coordinates **longitude** and **latitude**
<br>
### Login
Insert the **username** and **password** provided at the end of the log of Docker building procedure.

<br>
### Import data
Sibyl requires data packages *(.arix file extension) that contain inspection session data captured by the [Mission Controller]() application that manage and retrieves information during field inspection sessions such as the ARIX robot position, Video, RTR *(X-Ray scan)*, and [Eddyfi](https://www.eddyfi.com/) electromagnetic scan. This information is, later on, analyzed by the [ARIX Analysis]() application to generate reports that can be inspected, downloaded and exported by the [ARIX Report]() application. 

---
# Common Operations and Maintenance

## View Docker logs
Docker creates a virtual container with a volume in which al services are running. 

If you want to check the logs you can use the Linux Bash terminal command: `docker compose logs`

If you want to see only the tail of the Docker logs: `docker-compose logs -f`

## How to check system events
If you want to filter the Docker logs just by the event listener use the following command: `docker ps | grep event-listener` and then, `docker logs file-event-listener.sibyl/`

If you want to restart the event queue: `docker restart file-events-listener.sibyl`

## How to get the DB root user password
Remember the `.env` variable `MYSQL_RANDOM_ROOT_PASSWORD` is set to `yes`. That means the script will generate a random password while DB installation process. But, maybe you want to use any MySQL client like [DBeaver](https://dbeaver.io/) to inspect the DB tables and records.

For getting the automatic generated DB password of MySQL root user, after installation is complete, check de [Docker logs](##View-Docker-logs), using `docker logs aeb38034d3fd -f | grep PASSWORD` and find the line `GENERATED ROOT PASSWORD:`. Yous should find something like:
```
...
2021-11-18T14:47:36.906778Z 0 [System] [MY-010910] [Server] /usr/sbin/mysqld: Shutdown complete (mysqld 8.0.26-16)  Percona Server (GPL), Release 16, Revision 3d64165.
GENERATED ROOT PASSWORD: RUteP2Iq@b)aKtYKwebsUtDohim
mysqld: [Warning] World-writable config file '/etc/my.cnf.d/compatibility.cnf' is ignored.
...
```

## How to get the URL of Sibyl client application
As we mentioned before, Sibyl is a web based application, so you must use an internet browser to access it. If you Sibyl is deployed in any Web hosting or Cloud service you have to introduce the IP or the public URL declared in the DNS for such server. But, if you are deploying Sibyl in your local machine it will be deployed the Docker container will live inside a Volume and expose in a localhost URL *(Typically: 127.0.0.1)*.

If you followed these guidelines you should have some redirection mapped into your host file to avoid CORSS issues.

Now, it is possible to easily get the application host root path by the Bash command line, using the following command: `head -n1 .env`. It should return something like:
```
APP_HOST_ROOT=sibyl-next.docker
```
Then, insert `http://sibyl-nect.docker`in your internet browser to get access to the SIBYL client app.

If you experiment any issue with the previous, please, check the application server (Nginx) logs by using the Bash command: `docker logs nginx.sibyl`.

## How to restart the SIBYL Server
You have several ways to restart the SIBYL server using Docker. 

If you are using Docker Desktop or any Docker plugin for Visual Studio Code, you can use stop and play buttons to do so.

## Quick restart of SIBYL Server
For a quick restart of SIBYL server, you also can use the scripts provided in the project by running them from the Bash console: `scripts/down; scripts/up`

## How Docker re-start services
Note some services are defined as dependent on other ones. Remember all services are described in the `.env` file.

> You have to know from Docker v.3 when you kill or restart a parent service all dependent services will restart accordantly.

### Soft Restart
Then, for a **soft restart of SIBYL app** we have to restart the Application Server *(Nginx)*. Check the name of the NGINX at the `.env` file by using the Bash command: `vin .env` *(enter `:quick` for exit the inline editor)*. Find the variable called `NGINX_HOSTNAME`
```
NGINX_HOSTNAME=nginx.sibyl
```
That's the service name you have to restart by using the command `docker restart nginx.sybil`.

Then, you can run-up the application server running the script: `scripts/up nginx.sibyl`

### Hard Restart
There is another way to **hard restart the SIBYL app**. This is a more drastic way to restart the SIBYL services because it includes the virtual machine were the application server is running and execute the `docker-compose` up command.

For doing it, you can use the Bash command: `docker stop nginx.sibyl; docker vm nginx.sibyl; docker-compose up -d nginx.sibyl`  

## Version Control and Upgrades
Note that Docker collects images from local and/or remote repositories to build all containers and manage them inside a Volume of its Virtual Machine. Then, Docker uses the `docker-compose.yml`file to do so.

The `docker-compose.yml` file contains the receipt to build and deploy all services.

Note that most of that receipt values are addressed to environment variables of the `.env-version` file that contains the tags at the repositories to download and build every service/component. That file contains something like this:
```
MYSQL_VERSION=8
DATABASE_MIGRATIONS_VERSION=2.0.0-0.23.0__20211108171528
MINIO_VERSION=RELEASE.2021-04-27T23-46-03Z.release.0033eb96
MINIO_CONSOLE_VERSION=v0.7.1
MINIO_CONFIGURATOR_VERSION=RELEASE.2021-04-22T17-40-00Z
SECURITY_SERVICE_VERSION=2.0.0-0.4.0
REST_API_VERSION=0.20.4
FILE_ACCESS_PRE_SIGNER_VERSION=3.0.0-0.4.3
FILE_EVENTS_LISTENER_VERSION=2.0.0-0.3.0
DATA_IMPORT_PROCESS_MANAGER_VERSION=1.0.0-0.8.3
DATA_IMPORT_WORKERS_VERSION=0.13.3
ASSETS_GENERATION_SERVICE_VERSION=0.1.4
UI_APPLICATIONS_VERSION=develop-20211109.01
``` 

So, it is **very important to check software versions are compatible between them and updated**, specially the ones that are strict part of the Sibyl project and are developed and maintained by us. To do so, you can visit the [GitLab Sibyl project](https://gitlab.sibyl.arix-tech.com/SIBYL). Then check the latest tag of the branch `/develop`. It contains the reference you have to include in the `.env-versions` file (i.e. for the `DATABASE_MIGRATIONS_VERSION` it could be `2.0.0-0.24.0+20211112105350`) 

Then, you have to:

1. **Perform checkout of new software versions in your local repository**
2. **Edit the .env-version.yml file** to include the new versions according their latests tags.
3. **Run the script to build and restart the Docker container**: `scripts/up --build && scripts/post-create`


## How to clean all Docker containers
In some cases, it could be necessary to remove al Docker containers related with SIBYL. You can use both, the options of the Docker Desktop, or even some Docker plugins for Visual Studio Code or just the command line.

If you need to use the Bash command line to remove all Docker containers to **restart a clean build from scratch of the Docker container**, first, please read to the "Purge" section of the `README.md` at the [Docker-compose_build project of GitLab](https://gitlab.sibyl.arix-tech.com/SIBYL/docker-compose_build/-/blob/develop/README.md#purge)

Then, proceed executing the following script with in the Bash command line at the root path of the `/docker-compose_build` folder: `scripts/down -v --remove-orphan`

After that, you can launch everything from scratch by using the script: `scripts/up`


---
# Managing Changes
For instructions about how to manage changes in the project, please, visit the following section of the [README.md file at the `docker-compose_build`project in GitLab](https://gitlab.sibyl.arix-tech.com/SIBYL/docker-compose_build/-/blob/develop/README.md#changes).

---
# Managing DB migrations
There is a [GitLab project for managing DB migrations](https://gitlab.sibyl.arix-tech.com/SIBYL/database-migrations)

For creating a new migration (i.e. Insert a the refinery data). You can follow these instructions:

1. **Checkout a new branch** from your local repo `/db-migrations` using git command line: `git checkout -b migrationname`

2. **Generate a new migration sh script** using the Bash command line: `./gen.sh`. You should see the migration version number with two SQL scripts. For example:
    ```
    GENERATED MIGRATIONS 20211015113947
    /Users/USER$/code/arix/sibyl-next/database-migrations/20211015113447.do.sql
    /Users/USER$/code/arix/sibyl-next/database-migrations/20211015113947.undo.sql
    ```

3. Now, go to the migrations folder to **edit the sql scripts** and insert your SQL query code. For example:
    **20211015113447.do.sql**
    ```sql
    BEGIN;
        INSERT INTO 'Refinery' ('id', 'name','address','location') VALUES (1, 'Shell Chemicals', '5900 TX-225, Deer Park, TX 77536, USA', ST_GeomFromText('POINT(45, 16)'));
    COMMIT;
    ```

    **20211015113947.undo.sql**
    ```sql
    BEGIN;
        DELETE FROM 'Refinery' WHERE 'id'=1;
    COMMIT;

    ```

4. **Add the new files to the new migration branch** you just created: 
        `git add`
        `git status`
<br>
5. And **commit them** adding any descriptive comment: `git commit -m "adding and deleting the refinery"
<br>
6. Create a tag, if needed.
<br>
7. Update your `.env-versions` file and run the Docker scripts to update and restart your local/remote containers.


---
# Example of `.env` file
For edit or checking the dependencies you have to open the `.env`. You can do that with any text file editor, but you have to check that editor doesn't change the encoding and have LF activated for the proper line breaks. Otherwise lines will be concatenated and results on reading errors.

An easy way to edit files inline on the Bash terminal is using `.vin`. 

So you can use `vin .env` to edit the `.env`. It should has a content similar than the following one *(note: passwords and keys should be different for every environment)*

```
APP_HOST_ROOT=sibyl-next.docker

MYSQL_HOSTNAME=database.sibyl
MYSQL_RANDOM_ROOT_PASSWORD=yes
MYSQL_ONETIME_PASSWORD=
MYSQL_ROOT_PASSWORD=uTh4phaith0phee6Ahweeda5shooPeima1gaikoh
MYSQL_DATABASE=sibyl
MYSQL_USERNAME=sibyl
MYSQL_PASSWORD=uTh4phaith0phee6Ahweeda5shooPeima1gaikoh

DATABASE_MIGRATIONS_HOSTNAME=database-migrations.sibyl

JWT_SECRET=jaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a

USER_ASSET_GENERATION_WORKER_USERNAME=worker
USER_ASSET_GENERATION_WORKER_PASSWORD=WorkerPass1!

MINIO_HOSTNAME=storage.sibyl
MINIO_PUBLIC_SUBDOMAIN=storage
MINIO_ADMIN_ACCESS_KEY=qaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a
MINIO_ADMIN_SECRET_KEY=qaiy9sei4ainu6waiqu3ozie7eac1Hierao3Siod
MINIO_CONSOLE_ACCESS_KEY=jaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a
MINIO_CONSOLE_SECRET_KEY=saiy9sei4ainu6waiqu3ozie7eac1Hierao3Siod
MINIO_CONSOLE_PBKDF_PASSPHRASE=vaitie9aphuuhu5oopaehagh5ohquooluaQuoo1a
MINIO_CONSOLE_PBKDF_SALT=vaiy9sei4ainu6waiqu3ozie7eac1Hierao3Siod

# bucket names should not contain + signs since they will be URL encoded
MINIO_UNPACK_BUCKET=unpack
MINIO_UPLOADS_BUCKET=uploads-v2
MINIO_REPORTS_BUCKET=reports

MINIO_CONSOLE_HOSTNAME=minio-console.sibyl

MINIO_CONFIGURATOR_HOSTNAME=minio-configurator.sibyl

ACTIVEMQ_HOSTNAME=activemq.sibyl
ACTIVEMQ_WS_PUBLIC_SUBDOMAIN=ws
ACTIVEMQ_ADMIN_PASSWORD=yurVkgp3GiQ1yFmkfyF3E2zgQ3HjopJMj8aGkisi
ACTIVEMQ_API_USERNAME=sibyl
ACTIVEMQ_API_PASSWORD=yurVkgp3GiQ1yFmkfyF3E2zgQ3HjopJMj8aGkisi
ACTIVEMQ_MINIO_USERNAME=sibyl
ACTIVEMQ_MINIO_PASSWORD=yurVkgp3GiQ1yFmkfyF3E2zgQ3HjopJMj8aGkisi
ACTIVEMQ_VT_FILE_BUCKET_EVENTS=VirtualTopic.FileBucketEvents
ACTIVEMQ_VT_FILE_BUCKET_EVENTS_CQ_FILE_EVENTS_LISTENER=Consumer.FileEventsListener.VirtualTopic.FileBucketEvents
ACTIVEMQ_VT_FILE_EVENTS=VirtualTopic.FileEvents
ACTIVEMQ_VT_FILE_EVENTS_CQ_DATA_IMPORT=Consumer.DataImport.VirtualTopic.FileEvents
ACTIVEMQ_VT_PACKAGE_PROCESSING_EVENTS=VirtualTopic.PackageProcessingEvents
ACTIVEMQ_VT_PACKAGE_PROCESSING_EVENTS_CQ_DATA_IMPORT=Consumer.DataImport.VirtualTopic.PackageProcessingEvents
ACTIVEMQ_Q_PACKAGE_PROCESSING_STEPS_SCHEDULE=PackageProcessingStepsSchedule
ACTIVEMQ_Q_ASSETS_GENERATION_SCHEDULE=AssetsGenerationSchedule

NGINX_HOSTNAME=nginx.sibyl

SECURITY_SERVICE_HOSTNAME=security-service.sibyl
SECURITY_SERVICE_PUBLIC_SUBDOMAIN=security

REST_API_HOSTNAME=rest-api.sibyl
REST_API_PUBLIC_SUBDOMAIN=api

FILE_ACCESS_PRE_SIGNER_HOSTNAME=file-access-pre-signer.sibyl
FILE_ACCESS_PRE_SIGNER_PUBLIC_SUBDOMAIN=file-access

FILE_EVENTS_LISTENER_HOSTNAME=file-events-listener.sibyl

DATA_IMPORT_PROCESS_MANAGER_HOSTNAME=data-import-process-manager.sibyl

DATA_IMPORT_WORKERS_HOSTNAME=data-import-workers.sibyl

ASSETS_GENERATION_SERVICE_HOSTNAME=assets-generation-service.sibyl

UI_APPLICATIONS_HOSTNAME=ui-applications.sibyl

```



